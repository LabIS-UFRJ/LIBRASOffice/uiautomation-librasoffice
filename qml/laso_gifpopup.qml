import QtQuick 2.1
import QtQuick.Window 2.15


Window {
    id: librasoffice
    width: Screen.desktopAvailableWidth/100 * 15
    height: Screen.desktopAvailableHeight/100 * 35
    visible: false
    flags: Qt.Popup
    y: Screen.desktopAvailableHeight - height
    x: Screen.desktopAvailableWidth - width

    property int time_count: 0
    property string f_eventCatched: ""

    function delay(duration) { // In milliseconds
        var timeStart = new Date().getTime();

        while (new Date().getTime() - timeStart < duration) {
            // Do nothing
        }
    }

    Rectangle {
        id: rectangle
        width: parent.width
        height: parent.height
        color: "white"
        AnimatedImage {
            id: gif
            width: rectangle.width
            height: rectangle.height/100 * 85
        }
        Rectangle {
            property int frames: gif.frameCount
            width: 4; height: 8
            x: (gif.width - width) * gif.currentFrame / frames
            y: gif.height
            color: "red"
        }
        Rectangle {
            id: rectangle_legenda
            property string legenda: ""
            width: rectangle.width
            height: rectangle.height/100 * 10
            y: gif.height + 8
            Text{
                //width: parent.width
                //height: parent.height
                font.capitalization: Font.Capitalize
                fontSizeMode: Text.Fit
                minimumPointSize: 3
                text: rectangle_legenda.legenda
                font.family: "Helvetica"
                font.pointSize: 12
                font.bold: true
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: rectangle_legenda.verticalCenter
            }
        }
    }

    Timer {
        id: timer
        interval: 80; repeat: true
        running: true
        triggeredOnStart: true
        onTriggered: {
            if(laso_backend.getEventDetected().trim() !== "" && f_eventCatched.trim() !== laso_backend.getEventDetected().trim()){
                f_eventCatched = laso_backend.getEventDetected();
                console.log(f_eventCatched)
                if(laso_backend.database_contains(f_eventCatched)){
                    //delay(100)
                    if(laso_backend.getShowingSigns()){
                        librasoffice.time_count = 0;
                        rectangle_legenda.legenda = f_eventCatched;
                        console.log("umq vez so");
                        gif.source = "qrc:/gifs/" + f_eventCatched + ".gif";
                        librasoffice.visible = true;
                    }
                }
                else{
                    gif.source = "qrc:/gifs/sem sinal.gif";
                    rectangle_legenda.legenda = "Sem sinal: " + f_eventCatched;
                    librasoffice.visible = true;
                }
            }
            else{
                if(librasoffice.time_count <= 125){
                    librasoffice.time_count += 1;
                }
                else{
                    //librasoffice.f_eventCatched = "";
                    librasoffice.visible = false;
                }
            }
        }
    }
}


