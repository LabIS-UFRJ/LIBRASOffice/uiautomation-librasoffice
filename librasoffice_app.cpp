#include "librasoffice_app.h"


LIBRASOffice_App::LIBRASOffice_App(int argc, char *argv[]) : QGuiApplication(argc, argv){
    //add events to WinEventListener and move to listener thread
    listener->addEventsToIdentify(eventsToIdentify);
    listener->listenerStart();

    //start ui (systemTray and signs exibition)
    initUI();
}

void LIBRASOffice_App::initUI(){
    //Create a qml component for System tray and load popup to signs exibition
    engine->rootContext()->setContextProperty("laso_backend", this);

    QQmlComponent librasoffice_systray(engine, url_systray);
    librasoffice_systray.create();

    engine->load(url_gifpopup);
}

QString LIBRASOffice_App::getEventDetected(){
    return listener->getEventDetected()["EventName"].toString();
}

bool LIBRASOffice_App::database_contains(const QString &eventName){
    return QFile::exists(":/gifs/" + eventName + ".gif");
}

bool LIBRASOffice_App::getShowingSigns(){
    return showingSigns;
}

void LIBRASOffice_App::handleSignalExibition(const bool &controlValue){
    if (controlValue == false){
        showingSigns = false;
        qDebug() << "Signs exibition stopped";
    }
    else{
        showingSigns = true;
        qDebug() << "Signs exibiton started";
    }
}

void LIBRASOffice_App::run(){
    this->exec();
}
