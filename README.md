# librasoffice-win

# Introdução

# O Padrão Observer

Fazendo uso de sua definição formal, o Padrão Observer "*define uma dependência um-para-muitos entre os objetos de modo que, quando um objeto muda de estado, todos os seus dependentes são notificados e atualizados automaticamente*”. Isso quer dizer que esse padrão de projeto permite a integração entre dois ou mais objetos que interajam um com o outro, mas sem criar uma forte dependência entre eles.
Mas o que isso quer dizer?
O Padrão Observer adere ao Princípio da Ligação Leve, em que se busca designs levemente ligados. Isso quer dizer que os objetos podem interagir entre si normalmente, mas sabendo muito pouco um do outro. Essa característica garante uma maior flexibilidade de projeto, já que podemos adicionar, substituir, alterar ou remover Observers no projeto sem alterar o comportamento ou a estrutura do Subject.

Dentro do LIBRASOffice, utilizamos esse padrão na integração entre o LibreOffice e a biblioteca de GIFs associados a cada comando. Quando passamos o mouse sobre um ícone, o nosso software filtra o nome (ou a legenda que aparece sob ele, caso seja uma imagem) e busca pela tradução em nossa biblioteca, exibindo a imagem correspondente no canto inferior da tela. Caso a palavra buscada não esteja contida em nosso acervo, o GIF exibido corresponde a mensagem "*Esse sinal ainda não foi gravado*".

# As APIs de Acessibilidade

Essas são as ferramentas que nos possibilitam implementar o padrão Observer a nível de sistema operacional e que consistem no resgate e captura de eventos e demais interações com os elementos de interface do computador. Isso significa, portanto, que quando o mouse passa em cima de uma *tooltip*, por exemplo, o sistema, se tiver o recurso de acessibilidade instalado, irá registrar tal ocorrência (geralmente com informações de posicionamento de mouse) programa no qual o evento aconteceu, entre demais informações que podem ser úteis para o processo de criação de uma aplicação de acessibilidade como o LIBRASOffice - *screen readers* mundo afora também fazem uso desses dados para o seu funcionamento.

Sabendo disso, falemos então brevemente sobre as APIs em si. Naturalmente, cada uma das duas versões do LIBRASOffice utiliza a sua própria interface, desenvolvida de acordo com a sua própria arquitetura. 

No Windows, temos a utilização da [UI Automation](https://docs.microsoft.com/en-us/windows/win32/winauto/entry-uiauto-win32) - que sucede uma especificação anterior, conhecida como MSAA.

Já no Linux, em especial em ambientes GNOME, a interface predominante é a [ATSPI](https://gitlab.gnome.org/GNOME/at-spi2-core/-/blob/master/README), que opera em cima de um "mecanismo de comunicação entre processos" chamado [DBus](https://www.freedesktop.org/wiki/Software/dbus/). 

Cada uma dessas duas ferramentas é abordada com mais detalhes na sua respectiva documentação, onde serão consideradas questões como instalação e configuração.

# Submódulos

Submódulos são bem intuitivos - nada mais são do que repositórios dentro de um outro repositório. Sua utilização é, sobretudo, definida pela arquitetura do projeto, podendo prover certa independência a diferentes partes de uma aplicação, o que, por sua vez, facilita a manutenção e a identificação de erros.

## Gifs

A nossa biblioteca de GIFs conta com uma coletânea de arquivos de imagem, preparados pela nossa equipe com a ajuda de um tradutor humano. Neles, traduzimos visualmente os principais comandos do LibreOffice para libras, e é nesse repositório que o nosso software realiza as buscas mencionadas ao final da seção "Padrão Observer". O projeto intenciona a total tradução do LibreOffice, mas priorizamos as funcionalidades mais utilizadas.

## Windows Listener ([link](https://gitlab.com/LabIS-UFRJ/LIBRASOffice/eventlistener-win))

Esse módulo é responsável por implementar uma abstração de um *listener* em sistemas Windows, já pensando na formatação com que a API de acessibilidade da Microsoft emite seus eventos. O README disponível no repositório demonstra o seu uso e pode ser um bom ponto de partida para compreender o que o LIBRASOffice utiliza por trás dos panos.  

# O que é o QT? ([link](https://www.qt.io/product))

Qt é um framework multiplataforma para desenvolvimento de interfaces gráficas em C++ criado pela empresa norueguesa Trolltech. Com ele, é possível desenvolver aplicativos e bibliotecas uma única vez e compilá-los para diversas plataformas sem que seja necessário alterar o código fonte. 

Famoso no mundo open source - apesar de recentemente ter sido adquirido por uma empresa, o que acabou por tornar alguns de seus recursos privados - o Qt ganha destaque por sua versatilidade e variedade de ferramentas, possibilitando-o a ser utilizado no desenvolvimento de softwares desktop, mobile e até mesmo os "embarcados", como é o caso de aplicações para carros e outros sistemas de eletrodomésticos inteligentes.

Apesar de sua concepção em C++, seu uso provocou a criação de vários bindings - algo similar a um porte - para várias linguagens, como é o caso do Python. No LIBRASOffice, utilizamos essas duas linguagens, uma em cada versão, mas sempre compartilhando das ferramentas que o framework Qt nos proporciona.

## QML ([link](https://www.notion.so/Arquitetura-Geral-a8f87b89649d4391a6ea838935a94180))

QML é uma linguagem de marcação de texto, assim como o HTML, voltado para a criação de interfaces gráficas. Como a letra 'Q' acusa, a linguagem foi criada pensando no ambiente de desenvolvimento do Qt, a fim de trazer mais fluidez e dinâmica nos elementos gráficos das aplicações. 

Sua estrutura de código é bastante similar à do JSON, enquanto muitas das suas propriedades e lógicas de customização nos remetem imediatamente ao CSS. Veja um exemplo do próprio projeto abaixo.

![doc_images/qml_example.png](doc_images/qml_example.png)

# Dependências e Programas

## Baixando o projeto

Em primeiro lugar, baixamos o projeto e seus submódulos de maneira tradicional. Abra um terminal e rode os comandos:

```bash
$ git clone https://gitlab.com/LabIS-UFRJ/LIBRASOffice/uiautomation-librasoffice

#Com o projeto já baixado, dentro dele, rode:

$ git submodule init
$ git submodule update
```

Na versão de Windows, os submódulos são dois: o de gifs de sinais e aquele que contém uma implementação de um Listener para os eventos de UI-Automation, criado pelo Miguel. Nós falaremos mais sobre eles em breve.

## QT Creator

O QT Creator é uma IDE voltada diretamente para o desenvolvimento de aplicações com a framework Qt. Para facilitar nosso processo de debugging e de configuração, além de compilação, recomendamos fortemente o seu download.

Vamos comentar passos importantes do processo. 

1 - O download pode ser feito ao final da página [https://www.qt.io/download-open-source](https://www.qt.io/download-open-source). Uma questão a se ressaltar que esse download não é diretamente do programa que desejamos, mas sim do gerenciador de instalações do Qt. 

![Screenshot_1.png](doc_images/Screenshot_1.png)

2 - Após o download, execute o arquivo. Você terá de criar uma conta na plataforma se ainda não tiver uma.

3 - Vamos então à parte de licenças, que constitui uma informação importante: nos dias de hoje, o Qt é uma ferramenta proprietária - porém, uma parcela de seus recursos ainda continuam livres para uso, contanto que sob a devida lembrança do escopo de licença. Nessa etapa também é pedida a instituição que está atuando no projeto, que acaba sendo o LABIS.

4 - Agora começa a etapa mais importante, que é definir quais são os recursos que vamos baixar. São inúmeras bibliotecas e ferramentas para auxiliar o desenvolvimento de tantas outras aplicações possíveis, e para economizar nosso processamento e memória, vamos considerar somente o necessário para o LIBRASOffice. 

Na tela abaixo, escolha a instalação customizada e prossiga.

![Screenshot_2.png](doc_images/Screenshot_2.png)

Vamos então ser apresentados aos recursos disponíveis para download. Veja o que está abaixo da setinha do Qt, escolha a versão 5.15.2 (que foi a utilizada durante o desenvolvimento) e selecione os módulos sinalizados:

![Screenshot_3.png](doc_images/Screenshot_3.png)

Recolha o menu drop-down da versão e abra o último ponto, para ver o que foi pré-selecionado para as Developer and Designer Tools. As ferramentas marcadas abaixo são suficientes:

![Screenshot_4.png](doc_images/Screenshot_4.png)

E pronto! Podemos prosseguir normalmente com tudo até o download da framework ser realizado. Já aproveita pra pegar um café, porque vai demorar!

## Microsoft Visual Studio 2013

Talvez a instalação do Qt seja afetada tanto pela ausência do Visual Studio Community como das bibliotecas de C++ que costuma acompanhá-lo. Bom, se você conhece o Visual Studio, já pode imaginar que o Community é também um IDE - e de fato, o é. Ele acaba sendo a IDE base para o Qt construir a sua, e por isso a presença do Community de suas ferramentas também se faz importante. Vá no site [https://visualstudio.microsoft.com/pt-br/downloads/](https://visualstudio.microsoft.com/pt-br/downloads/) e selecione o modelo "Comunidade", como mostrado na imagem abaixo.

![Screenshot_5.png](doc_images/Screenshot_5.png)

Sua instalação é simples e direta, e já costuma vir com uma quantidade de recursos balanceada para nosso poderio computacional. 

## Inno Setup

Por último, devemos instalar um terceiro programa, e calma lá: dessa vez não é uma IDE. O Inno Setup (open source) é responsável por pegar a versão compilada com o QtCreator e criar um executável Windows a partir dele - o que é fundamental para a distribuição do LIBRASOffice. Para tal, nós precisamos fornecer um script de informações sobre o executável, que já vêm junto do nosso repositório. Vale lembrar que esse programa tem mais destaque na seção de criação de Build.

Seu download é o mais simples de todos, e pode ser feito no site [https://jrsoftware.org/isdl.php#stable](https://jrsoftware.org/isdl.php#stable). Basta selecionar um dos links da seção de lançamentos estáveis para baixar o seu programa. 

![Screenshot_6.png](doc_images/Screenshot_6.png)

# Código

## Estrutura do C++ (Headers e arquivos cpp)

Projetos em C e C++ podem ser criados com alguns tipos de padrão, e o que foi usado no LIBRASOffice é característico da separação entre arquivos de **declaração** (chamados de headers, com o sufixo .h) e de **implementação** (com sufixo .cpp).

Dessa forma, uma classe, por exemplo, pode ter seus atributos e métodos declarados em um arquivo, enquanto suas definições são descritas em outro. Se desejar, uma discussão interessante e introdutória sobre o assunto pode ser vista em [https://stackoverflow.com/questions/1305947/why-does-c-need-a-separate-header-file](https://stackoverflow.com/questions/1305947/why-does-c-need-a-separate-header-file).

Para finalizar, vamos ver rapidamente um exemplo:

 

```cpp
//No arquivo librasoffice_app.h, dentro da classe, encontra-se a declarção de um método:
Q_INVOKABLE void handleSignalExibition(const bool &value);

//A sua implementação correspondente é encontrada no arquivo librasoffice_app.cpp
void LIBRASOffice_App::handleSignalExibition(const bool &controlValue){
    if (controlValue == false){
        showingSigns = false;
        qDebug() << "Signs exibition stopped";
    }
    else{
        showingSigns = true;
        qDebug() << "Signs exibiton started";
    }
}
```

## librasoffice_app.h e librasoffice_app.cpp

Ainda que estejam separadas, as declarações e definições estão intimamente relacionadas. Portanto, faz muito mais sentido abordar os trechos de código de ambos que são relativos a uma mesma feature. Por conta disso, vamos analisar cada parcela do código de maneira conjunta, sempre indicando de onde vem cada trecho.

### Engine e carregamento de elementos gráficos

Como vimos acima, é aqui que ocorrem as declarações de nossas classes e seus atributos e métodos. De forma muito similar à versão do programa para Linux, nós temos aqui algumas presenças importantes, como a lista de eventos emitidos pela API da UI-Automation que desejamos capturar, a criação de uma instância de uma engine junto ao carregamento de objetos qml e claro, a criação de nosso listener.

falar depois sobre o ui-automation por preguicinha

Logo abaixo dos eventos, temos a seguinte linha:

```cpp
//librasoffice_app.h
QQmlApplicationEngine *engine = new QQmlApplicationEngine;
```

Aqui, ocorre a criação de um objeto da classe *[QQmlApplicationEngine](https://doc.qt.io/qtforpython-5/PySide2/QtQml/QQmlApplicationEngine.html).* Esse objeto viabiliza a inclusão e administração de recursos gráficos feitos em QML na aplicação em Qt. No arquivo de implementação, vemos que algumas outras partes do código interagem com a engine. A função *LIBRASOffice_App::initUI* é central nesse processo. 

A primeira linha do método já levanta uma questão importante, o chamado [contexto](https://doc.qt.io/qt-5/qqmlcontext.html). Podemos entender esse conceito como se fosse um escopo, nos quais os elementos gráficos e determinados valores da aplicação podem ser interpoladas (que acaba por ser outro recurso da engine). Assim, por exemplo, é possível passar o tempo de um gif, verificado em código, para uma variável no arquivo qml, o qual exibiria um temporizador.

Sabendo disso, podemos expor os dados de nosso backend à engine criada, utilizando o método *[setContextProperty.](https://doc.qt.io/qt-5/qqmlcontext.html)*

Logo depois, é criado um componente com o ícone de system tray de nosso programa. Esse termo nada mais é do que a nomenclatura da parte direita da barra de gerenciamento de tarefas, onde é possível selecionar uma rede de internet e desativar temporariamente alguns programas - é por esse motivo, inclusive, que ele está lá para o LIBRASOffice.

Por último, a engine também é alimentada com o arquivo qml de gif

```cpp
//librasoffice_app.h

//São criados caminhos para os elementos qml
const QUrl url_systray = QStringLiteral("qrc:/qml/laso_systray.qml");
const QUrl url_gifpopup = QStringLiteral("qrc:/qml/laso_gifpopup.qml");

// =======================================================================================

//librasoffice_app.cpp
void LIBRASOffice_App::initUI(){
    //Create a qml component for System tray and load popup to signs exibition
    engine->rootContext()->setContextProperty("laso_backend", this);

    QQmlComponent librasoffice_systray(engine, url_systray);
    librasoffice_systray.create();

    engine->load(url_gifpopup);
}
```

### Listener e detecção de eventos

A API de Acessibilidade do Windows é chamada de UI-Automation. A documentação da Microsoft de seus próprios recursos costuma ser bem direta e funcional, portanto, a melhor forma de conhecer mais sobre a interface é através de sua [leitura](https://docs.microsoft.com/en-us/windows/win32/winauto/entry-uiauto-win32). Na aba à esquerda é possível ver vários artigos focados em aspectos específicos que podem vir a complementar sua pesquisa em algum momento posterior.

Da mesma maneira que a versão do Linux, nosso intuito nesse momento é capturar eventos emitidos pela UI-Automation. Para tal tarefa, foi criado um repositório mais abstrato, implementado pelo mesmo desenvolvedor do LIBRASOffice: [https://gitlab.com/LabIS-UFRJ/LIBRASOffice/eventlistener-win](https://gitlab.com/LabIS-UFRJ/LIBRASOffice/eventlistener-win). O repositório já possui uma documentação e também é ilustrado com um exemplo na página principal. Teste a captura de eventos com o seu navegador, por exemplo, para se familiarizar com as impressões.

```cpp
//No arquivo librasoffice.h
WinEventListener *listener = new WinEventListener("soffice.bin");
```

Aqui vale a pena mencionar também os eventos que de fato queremos monitorar, passados para o filtro do nosso listener, como descrito abaixo:

```cpp
QList<QString> eventsToIdentify = {
        "UIA_AutomationFocusChangedEventId",
        "UIA_ToolTipOpenedEventId",
        "UIA_Window_WindowClosedEventId",
    };
```

Por último, aqui vai uma [referência mais direcionada](https://docs.microsoft.com/en-us/windows/win32/winauto/uiauto-eventsoverview) aos eventos do UI-Automation caso seja necessário para a evolução ou criação de alguma feature no futuro, bem como para consulta das categorias citadas acima.

## Outros arquivos

### qml.qrc

Como já sabemos, o LIBRASOffice não utiliza banco de dados como forma de armazenamento de sinais, mas sim um submódulo do Git. Porém, da maneira em que a implementação está no momento, também é necessário utilizar um sistema de inclusão de recursos associado a itens de multimídia estáticos, como é o caso dos nossos gifs e de elementos como ícones e telas de qml. Utiliza-se então um arquivo qrc, que deriva do modelo [Qt Resource System](https://doc.qt.io/qt-5/resources.html).

Há ainda uma consideração a ser feita: não no momento, automatização desse processo, o que significa que todo e qualquer sinal vai precisar ser incluído manualmente nesse arquivo. Veja uma mostra dele para entender seu formato, bem similar a linguagens de marcação de texto.

```cpp
<!DOCTYPE RCC>
<RCC version="1.0">
   <qresource prefix="/">
      <file>qml/laso_gifpopup.qml</file>
      <file>qml/laso_systray.qml</file>
      <file>qml/assets/images/labis-librasoffice.png</file>
      <file>librasoffice.ico</file>
      <file>gifs/abrir.gif</file>
      <file>gifs/abrir arquivo remoto.gif</file>
      <file>gifs/abrir modelo.gif</file>
      <file>gifs/abrir no modo design.gif</file>
...
```

# Build do arquivo executável do Windows

**1** - Abra o QtCreator Community e nele, carregue o arquivo *CMakeLists.txt,* baixado junto ao repositório*.* Apenas com tal arquivo, o QT já consegue identificar todo o projeto e portanto, montar a sua estrutura no gerenciador de arquivos do programa.

**2** - Depois, aperte o botão de "play", no canto inferior esquerdo *(Figura 1),* o que vai rodar nosso projeto. Se tudo ocorrer corretamente, será gerada uma pasta com arquivos relativos ao build e a demais dependências, no mesmo diretório no qual está localizada a pasta do projeto em si *(Figura 2).*

![doc_images/botao.png](doc_images/botao.png)

                                                                                         Figura 1

![doc_images/pastas_build.png](doc_images/pastas_build.png)

                                                  Figura 2 *-* perceba que uma pasta foi gerada com o sufixo "build-"

**3** - Crie uma nova pasta - sem nenhuma restrição de nome, apesar da recomendação de algum título coerente com o processo. Este novo diretório será aquele que de fato nos gerar um executável instalado. Para isso, vamos precisar pôr alguns arquivos em seu escopo; são eles: 

- *LIBRASOffice-win.exe (arquivo) →* é encontrado dentro da pasta de build gerada.
- *qml (pasta)* → essa pasta contém todos os arquivos qml, responsáveis por configurações de interface gráfica. Pode ser encontrada na pasta *uiautomation-librasoffice*, na qual também encontramos os próximos dois itens dessa lista;
- *librasoffice.ico (arquivo) →* é o ícone do projeto;
- *script.iss (arquivo)* → arquivo de configuração do Inno, reponsável por reunir os demais itens em um instalador pronto para compartilhamento;
- crie um arquivo *README.txt* , contendo as informações tradicionais desse tipo de documento, como informações sobre o que é o projeto e tudo mais;
- quando funcionar, utilizar a parte de certificação;

![doc_images/arquivos-build.png](doc_images/arquivos-build.png)

                                  Figura 3 - Arquivos necessários à pasta de geração do instalador do Windows

**4** - Abra o PowerShell do Windows, navegue até o diretório que você criou para esta etapa e digite o comando a seguir: 

```powershell
windeployqt.exe --quick .
```

**5** - Por fim, abra o arquivo *script.iss* e execute-o.

![doc_images/inno-setup.png](doc_images/inno-setup.png)

Cobertos todos os processos de maneira correta e sem erros no caminho, teremos o tão esperado arquivo executável do Windows, o qual podemos veicular ao site a fim de que outras pessoas façam o seu download.

![doc_images/executavel_na_pasta.png](doc_images/executavel_na_pasta.png)

            Figura 4 - O executável estará localizado na mesma pasta em que criamos nossa pasta 

Dessa maneira, o projeto está pronto para a distribuição em máquinas Windows.
