#ifndef LIBRASOFFICE_APP_H
#define LIBRASOFFICE_APP_H

#include <eventlistener-win/wineventlistener.h>

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQmlContext>
#include <QUrl>
#include <QList>
#include <QString>
#include <QVariantMap>
#include <QQmlEngine>
#include <QFile>



class LIBRASOffice_App : QGuiApplication
{
    Q_OBJECT
    bool showingSigns = true;
    QList<QString> eventsToIdentify = {
        "UIA_AutomationFocusChangedEventId",
        "UIA_ToolTipOpenedEventId",
        "UIA_Window_WindowClosedEventId",
    };

    QQmlApplicationEngine *engine = new QQmlApplicationEngine;

    const QUrl url_systray = QStringLiteral("qrc:/qml/laso_systray.qml");
    const QUrl url_gifpopup = QStringLiteral("qrc:/qml/laso_gifpopup.qml");

    WinEventListener *listener = new WinEventListener("soffice.bin");

    QVariantMap eventDetected;

public:
    explicit LIBRASOffice_App(int argc, char *argv[]);
    Q_INVOKABLE void handleSignalExibition(const bool &value);
    Q_INVOKABLE QString getEventDetected();
    Q_INVOKABLE bool database_contains(const QString &eventName);
    Q_INVOKABLE bool getShowingSigns();
    void initUI();
    void run();
};

#endif // LIBRASOFFICE_APP_H
