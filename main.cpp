#include <librasoffice_app.h>
#include <QIcon>


int main(int argc, char *argv[])
{
     LIBRASOffice_App *librasoffice_app = new LIBRASOffice_App(argc, argv);
     librasoffice_app->run();
}
